import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { css } from 'glamor';

css.global('html', {width: "100%"})
css.global('body', {width: "100%"})

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

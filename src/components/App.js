import React, { Component } from 'react';
import { SudokuForm } from './SudokuForm';

class App extends Component {
  render() {
    return (
        <SudokuForm />
    );
  }
}

export default App;

import React, { Component } from 'react';
import _ from 'lodash'
import PropTypes from 'prop-types';
import { Square } from './Square';

export class Row extends Component {
    render() {
        let row = _.range(9).map(i => {
          let coordinates = `${i}, ${this.props.rowNumber}`;
          return <Square key={coordinates} coordinates={coordinates} formInfo={this.props.formInfo} />;
        });
        return (
            <tr key={`${this.props.rowNumber}`}>{row}</tr>
        );
    }
}

Row.propTypes = {
  rowNumber: PropTypes.number.isRequired,
  formInfo: PropTypes.object.isRequired
};

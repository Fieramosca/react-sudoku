import React, { Component } from 'react';
import { Button } from 'glamorous';

export class SolveButton extends Component {
  render() {
    return <Button onClick={this.props.onClick} className="btn btn-outline-primary" css={style} >Solve</Button>;
  }
}

const style = {
  margin: "auto",
  display: "block"
}
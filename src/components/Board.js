import React, {Component} from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Row } from './Row';
import { Table } from 'glamorous';


export class Board extends Component {
  render() {
    let rows = _.range(9).map(i => 
      <Row key={i.toString()} rowNumber={i} formInfo={this.props.formInfo} />
    );
    return (
      <Table margin="auto" >
        <tbody>
          {rows}
        </tbody>
      </Table>
    );
  }
}

Board.propTypes = {
  formInfo: PropTypes.object.isRequired
};
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Td, Input } from 'glamorous';

export class Square extends Component {
  constructor(props) {
    super(props);
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(inputEvent) {
    this.props.formInfo.onInput(this.props.coordinates, inputEvent.target.value);
  }

  render() {
    const [x, y] = this.props.coordinates.split(',').map(Number);
    return (
      <Td key={this.props.coordinates} css={squareStyle} border='1px solid black' >
        {this.props.formInfo.isSolved ? 
         this.props.formInfo.board[y][x] :
         <Input 
            type="number" id={this.props.coordinates} onInput={this.handleInput}
            css={squareStyle} border='none' 
         />}
      </Td>
    );
  }
}

Square.propTypes = {
  coordinates: PropTypes.string.isRequired,
  num: PropTypes.number,
  formInfo: PropTypes.shape({
    isSolved: PropTypes.bool.isRequired,
    onInput: PropTypes.func.isRequired,
    board: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))
  }).isRequired
};

Square.defaultProps = {
  num: 0
};

const squareSize = '5vmin';

const squareStyle = {
  width: squareSize,
  height: squareSize,
  textAlign: 'center',
  fontSize: 'large'
}
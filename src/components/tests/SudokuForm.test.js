import React from 'react';
import ReactDOM from 'react-dom';
import { SudokuForm} from '../SudokuForm';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SudokuForm />, div);
    ReactDOM.unmountComponentAtNode(div);
});
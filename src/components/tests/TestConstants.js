export const TestConstants = {
  mockFormInfo: {
    isSolved: true,
    onInput: (x) => x,
    board: Array(9).fill(Array(9).fill(0)),
  },
  mockRowNumber: 0,
  mockCoordinates: '0, 0'
}
import React from 'react';
import ReactDOM from 'react-dom';
import { Row } from '../Row';
import { TestConstants } from './TestConstants'

it('renders without crashing', () => {
    const tbody = document.createElement('tbody');
    ReactDOM.render(<Row rowNumber={TestConstants.mockRowNumber} formInfo={TestConstants.mockFormInfo} />, tbody);
    ReactDOM.unmountComponentAtNode(tbody);
}) 
import React from 'react';
import ReactDOM from 'react-dom';
import { Board } from '../Board';
import { TestConstants } from './TestConstants'

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Board formInfo={TestConstants.mockFormInfo}/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
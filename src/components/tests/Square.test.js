import React from 'react';
import ReactDOM from 'react-dom';
import { Square } from '../Square';
import { TestConstants } from './TestConstants';

it('renders without crashing', () => {
    const tr = document.createElement('tr');
    ReactDOM.render(<Square coordinates={TestConstants.mockCoordinates} formInfo={TestConstants.mockFormInfo} />, tr);
    ReactDOM.unmountComponentAtNode(tr);
});
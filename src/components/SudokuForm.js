import React, { Component } from "react";
import _ from 'lodash';
import { Board } from "./Board";
import { SolveButton } from "./SolveButton";
import glamorous from "glamorous";

export class SudokuForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      board: _.range(9).map(_ => Array(9).fill(0)),
      isSolved: false,
      failedToSolve: false,
      badInput: false,
    };
    this.handleSquareInput = this.handleSquareInput.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.processJson = this.processJson.bind(this);
  }

  handleSquareInput(coordinate, input) {
    const num = input === "" ? 0 : parseInt(input, 10);

    if (num > 9 || num < 0 || !Number.isInteger(num)) {
      this.setState({ badInput: true });
    } else {
      const [x, y] = coordinate.split(',').map(Number);
      this.setState(prevState => {
        const prevBoard = prevState.board;
        prevBoard[y][x] = num;
        return { 
          board: prevBoard,
          badInput: false
        };
      });
    }
  }

  processJson(json) {
    this.setState({
      board: json,
      isSolved: true
    });
  }

  handleClick() {
    fetch("http://localhost:8000/solve/" + JSON.stringify(this.state.board), { credentials: 'same-origin' }).then(
      response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      }).then(this.processJson)
      .catch(error => {
        console.log(this.state.board, error);
        this.setState({ failedToSolve: true });
      });
  }

  render() {
    let formInfo = {
      isSolved: this.state.isSolved,
      onInput: this.handleSquareInput,
      board: this.state.board
    };
    return (
      <SudokuFormDiv className="container" >
        <Board formInfo={formInfo} />
        { this.state.badInput && 
          <WarningP className='alert alert-warning' >Squares must contain numbers between 1 - 9</WarningP> }
        { this.state.failedToSolve && <WarningP className='alert alert-danger' >Sudoku is not solveable</WarningP> }
        <SolveButton onClick={this.handleClick} />
      </SudokuFormDiv>
    );
  }
}

const SudokuFormDiv = glamorous.div({
  margin: '0 auto',
  width: 'fit-content',
})

const WarningP = glamorous.p({
  fontSize: "1em"
})